json.extract! backend_notice, :id, :backend_categories_id, :title, :content, :created_at, :updated_at
json.url backend_notice_url(backend_notice, format: :json)
