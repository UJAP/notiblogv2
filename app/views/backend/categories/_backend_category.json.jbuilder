json.extract! backend_category, :id, :title, :created_at, :updated_at
json.url backend_category_url(backend_category, format: :json)
