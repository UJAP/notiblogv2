class Backend::Category < ApplicationRecord
	has_many :backend_notices, class_name: "Backend::Notice", foreign_key:"backend_categories_id" 
end
