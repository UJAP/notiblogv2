class Backend::Notice < ApplicationRecord
  belongs_to :backend_categories, class_name: "Backend::Category"
  belongs_to :user
end
