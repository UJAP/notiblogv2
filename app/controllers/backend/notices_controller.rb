class Backend::NoticesController < ApplicationController
  before_action :set_backend_notice, only: [:show, :edit, :update, :destroy]

  # GET /backend/notices
  # GET /backend/notices.json
  def index
    @backend_notices = Backend::Notice.all
  end

  # GET /backend/notices/1
  # GET /backend/notices/1.json
  def show
  end

  # GET /backend/notices/new
  def new
    @backend_notice = Backend::Notice.new
  end

  # GET /backend/notices/1/edit
  def edit
  end

  # POST /backend/notices
  # POST /backend/notices.json
  def create
    @backend_notice = Backend::Notice.new(backend_notice_params)
    #@backend_notice = current_user.backend_notice.build(backend_notice_params)
    respond_to do |format|
      if @backend_notice.save
        format.html { redirect_to @backend_notice, notice: 'Notice was successfully created.' }
        format.json { render :show, status: :created, location: @backend_notice }
      else
        format.html { render :new }
        format.json { render json: @backend_notice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backend/notices/1
  # PATCH/PUT /backend/notices/1.json
  def update
    respond_to do |format|
      if @backend_notice.update(backend_notice_params)
        format.html { redirect_to @backend_notice, notice: 'Notice was successfully updated.' }
        format.json { render :show, status: :ok, location: @backend_notice }
      else
        format.html { render :edit }
        format.json { render json: @backend_notice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backend/notices/1
  # DELETE /backend/notices/1.json
  def destroy
    @backend_notice.destroy
    respond_to do |format|
      format.html { redirect_to backend_notices_url, notice: 'Notice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_backend_notice
      @backend_notice = Backend::Notice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def backend_notice_params
      params.require(:backend_notice).permit(:backend_categories_id, :title, :content, :user)
    end
end
