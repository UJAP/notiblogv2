class CreateBackendNotices < ActiveRecord::Migration[5.1]
  def change
    create_table :backend_notices do |t|
      t.references :backend_categories, foreign_key: true
      t.references :user, foreign_key: true
      t.string :title
      t.text :content


      t.timestamps
    end
  end
end
