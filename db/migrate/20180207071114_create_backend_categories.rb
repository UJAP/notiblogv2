class CreateBackendCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :backend_categories do |t|
      t.string :titles
      
      t.timestamps
    end
  end
end
