Rails.application.routes.draw do
  namespace :backend do
    resources :notices
  end
  namespace :backend do
    resources :categories
  end
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #root to: "home#index"

end
