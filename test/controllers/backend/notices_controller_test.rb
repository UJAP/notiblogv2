require 'test_helper'

class Backend::NoticesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @backend_notice = backend_notices(:one)
  end

  test "should get index" do
    get backend_notices_url
    assert_response :success
  end

  test "should get new" do
    get new_backend_notice_url
    assert_response :success
  end

  test "should create backend_notice" do
    assert_difference('Backend::Notice.count') do
      post backend_notices_url, params: { backend_notice: { backend_categories_id: @backend_notice.backend_categories_id, content: @backend_notice.content, title: @backend_notice.title } }
    end

    assert_redirected_to backend_notice_url(Backend::Notice.last)
  end

  test "should show backend_notice" do
    get backend_notice_url(@backend_notice)
    assert_response :success
  end

  test "should get edit" do
    get edit_backend_notice_url(@backend_notice)
    assert_response :success
  end

  test "should update backend_notice" do
    patch backend_notice_url(@backend_notice), params: { backend_notice: { backend_categories_id: @backend_notice.backend_categories_id, content: @backend_notice.content, title: @backend_notice.title } }
    assert_redirected_to backend_notice_url(@backend_notice)
  end

  test "should destroy backend_notice" do
    assert_difference('Backend::Notice.count', -1) do
      delete backend_notice_url(@backend_notice)
    end

    assert_redirected_to backend_notices_url
  end
end
